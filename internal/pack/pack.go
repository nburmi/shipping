package pack

// calculate the minimum number of items from packs required to ship the given number of items.
func Pack(target int, packs []int) []int {
	largest := -1

	// one element slice
	for i, size := range packs {
		if target <= size {
			largest = i
			break
		}
	}

	add := []int{}
	if largest == -1 {
		// target is larger than the largest pack
		largest = len(packs) - 1

		count := target / packs[largest]
		for i := 0; i < count; i++ {
			add = append(add, packs[largest])
		}

		target = target - (count * packs[largest])
	}

	diff := packs[largest] - target
	// if diff is 0, then we have an exact match
	// if diff is less than packs[0], then we have a match with the next smallest pack
	if diff == 0 || diff < packs[0] {
		return []int{packs[largest]}
	}

	g := greedy(target, packs[:largest])
	return append(g, add...)
}

func findNearest(target int, packs []int) int {
	index := -1
	v := -1
	for i := len(packs) - 1; i >= 0; i-- {
		if packs[i] <= target && (v == -1 || target-packs[i] < v) {
			v = target - packs[i]
			index = i
		}
	}

	if index == -1 {
		return 0
	}

	return index
}

func greedy(target int, packs []int) []int {
	result := []int{}

	for target > 0 {
		index := findNearest(target, packs)
		if index == -1 {
			break
		}

		target = target - packs[index]
		result = append(result, packs[index])

		// remove the pack from the list
		packs = deleteSlice(packs, index)
	}

	return result
}

func deleteSlice(s []int, i int) []int {
	if i == 0 {
		return s[1:]
	}

	if i == len(s)-1 {
		return s[:i]
	}

	return append(s[:i], s[i+1:]...)
}

package pack

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPack(t *testing.T) {
	tests := []struct {
		name  string
		input int
		want  []int
		packs []int
	}{
		{
			name:  "one pack minimum",
			input: 1,
			want:  []int{250},
			packs: []int{250, 500},
		},
		{
			name:  "one pack next minimum",
			input: 251,
			want:  []int{500},
			packs: []int{250, 500},
		},
		{
			name:  "two packs",
			input: 501,
			want:  []int{250, 500},
			packs: []int{250, 500, 1000, 2000, 5000},
		},
		{
			name:  "two packs",
			input: 1001,
			want:  []int{1000, 250},
			packs: []int{250, 500, 1000, 2000, 5000},
		},
		{
			name: "three packs",
			// 1000 + 500 + 250
			input: 1700,
			want:  []int{1000, 500, 250},
			packs: []int{250, 500, 1000, 2000, 5000},
		},
		{
			name: "four packs",
			// 2000 + 1000 + 500 + 250
			input: 3700,
			want:  []int{2000, 1000, 500, 250},
			packs: []int{250, 500, 1000, 2000, 5000},
		},
		{
			name:  "one pack minimum",
			input: 1,
			want:  []int{250},
			packs: []int{250, 500, 1000, 2000, 5000},
		},
		{
			name:  "one pack next minimum",
			input: 251,
			want:  []int{500},
			packs: []int{250, 500, 1000, 2000, 5000},
		},
		{
			name:  "one pack",
			input: 12001,
			want:  []int{5000, 5000, 2000, 250},
			packs: []int{250, 500, 1000, 2000, 5000},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.ElementsMatch(t, tt.want, Pack(tt.input, tt.packs), "packs do not match")
		})
	}
}

func TestFindNearest(t *testing.T) {
	packs := []int{10, 20, 30, 40, 50}

	// Test case 1: target is less than the smallest pack size.
	if got, want := findNearest(5, packs), 0; got != want {
		t.Errorf("findNearest(5, packs) = %v, want %v", got, want)
	}

	// Test case 2: target is greater than the largest pack size.
	if got, want := findNearest(60, packs), 4; got != want {
		t.Errorf("findNearest(60, packs) = %v, want %v", got, want)
	}

	// Test case 3: target is equal to a pack size.
	if got, want := findNearest(30, packs), 2; got != want {
		t.Errorf("findNearest(30, packs) = %v, want %v", got, want)
	}

	// Test case 4: target is between two pack sizes.
	if got, want := findNearest(25, packs), 1; got != want {
		t.Errorf("findNearest(25, packs) = %v, want %v", got, want)
	}

	// Test case 5: target is equal to the largest pack size.
	if got, want := findNearest(50, packs), 4; got != want {
		t.Errorf("findNearest(50, packs) = %v, want %v", got, want)
	}
}

package transport

import (
	"fmt"
	"io/fs"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/nburmi/shipping/internal/transport/generated"
	"gitlab.com/nburmi/shipping/web"
)

func initSwagger(r chi.Router) error {
	swagger, err := generated.GetSwagger()
	if err != nil {
		return fmt.Errorf("failed to load swagger: %w", err)
	}

	// Clear out the servers array in the swagger spec, that skips validating
	// that server names match. We don't know how this thing will be run.
	swagger.Servers = nil

	r.Get("/swagger.json", func(w http.ResponseWriter, r *http.Request) {
		respond(w, http.StatusOK, swagger)
	})

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/static/swagger-ui/", http.StatusTemporaryRedirect)
	})

	fsys, err := fs.Sub(web.GetSwaggerUI(), "static")
	if err != nil {
		return fmt.Errorf("failed to load swagger static files: %w", err)
	}

	r.Handle("/static/*", http.StripPrefix("/static/", http.FileServer(http.FS(fsys))))

	return nil
}

package transport

import (
	"context"
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"

	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
	"gitlab.com/nburmi/shipping/internal/calculator"
	"gitlab.com/nburmi/shipping/internal/transport/generated"
)

func NewHTTP(c *calculator.Calculator) (http.Handler, error) {
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	srv := serverImpl{calc: c}
	options := generated.ChiServerOptions{BaseRouter: r}

	if err := initSwagger(r); err != nil {
		return nil, fmt.Errorf("init swagger: %w", err)
	}

	return generated.HandlerWithOptions(srv, options), nil
}

var _ generated.ServerInterface = serverImpl{}

type serverImpl struct {
	calc *calculator.Calculator
}

// calculate the number of packs for items.
// (POST /calculator)
func (s serverImpl) PostCalculator(w http.ResponseWriter, r *http.Request) {
	var items generated.PostCalculatorJSONBody

	err := decodeJSON(r, &items)
	if err != nil {
		logError(r.Context(), r, "decode json", err)
		respond(w, http.StatusBadRequest, generated.BadRequest{
			Message: err.Error(),
		})

		return
	}

	packs := s.calc.Calculate(items.Items)
	respond(w, http.StatusOK, packs)
}

// get current packs
// (GET /pack)
func (s serverImpl) GetPack(w http.ResponseWriter, r *http.Request) {
	packs := s.calc.Packs()

	response := make(generated.Packs, len(packs))
	for i, p := range packs {
		response[i] = p
	}

	respond(w, http.StatusOK, packs)
}

// override packs
// (PUT /pack)
func (s serverImpl) PutPack(w http.ResponseWriter, r *http.Request) {
	var packs generated.Packs
	err := decodeJSON(r, &packs)
	if err != nil {
		logError(r.Context(), r, "decode json", err)
		respond(w, http.StatusBadRequest, generated.BadRequest{
			Message: err.Error(),
		})

		return
	}

	s.calc.SetPacks(packs)
	respond(w, http.StatusOK, packs)
}

func decodeJSON[T any](r *http.Request, v T) error {
	err := json.NewDecoder(r.Body).Decode(v)
	if err != nil {
		return fmt.Errorf("decode json: %w", err)
	}

	err = r.Body.Close()
	if err != nil {
		return fmt.Errorf("close body: %w", err)
	}

	return nil
}

func respond[T any](w http.ResponseWriter, statusCode int, v T) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(statusCode)

	err := json.NewEncoder(w).Encode(v)
	if err != nil {
		slog.Warn("encode json", slog.String("error", err.Error()))
	}
}

func logError(ctx context.Context, req *http.Request, msg string, err error) {
	if err == nil {
		return
	}

	slog.WarnContext(ctx, msg,
		slog.String("url", req.URL.String()),
		slog.String("method", req.Method),
		slog.String("error", err.Error()),
		slog.Any("user-agent", req.UserAgent()),
	)
}

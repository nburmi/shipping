package calculator

import (
	"sort"
	"sync"

	"gitlab.com/nburmi/shipping/internal/pack"
)

type Calculator struct {
	packs []int
	mu    sync.RWMutex
}

func New(packs []int) *Calculator {
	return &Calculator{
		packs: packs,
	}
}

func (c *Calculator) Calculate(input int) []int {
	c.mu.RLock()
	defer c.mu.RUnlock()

	return pack.Pack(input, c.packs)
}

func (c *Calculator) Packs() []int {
	c.mu.RLock()
	defer c.mu.RUnlock()

	return c.packs
}

func (c *Calculator) SetPacks(packs []int) {
	unique := map[int]struct{}{}
	for _, p := range packs {
		unique[p] = struct{}{}
	}

	packs = make([]int, 0, len(unique))
	for p := range unique {
		packs = append(packs, p)
	}

	sort.Ints(packs)

	c.mu.Lock()
	defer c.mu.Unlock()

	c.packs = packs
}

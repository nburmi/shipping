
install-dependencies:
	go install github.com/deepmap/oapi-codegen/cmd/oapi-codegen@v1.13.4; # openapi generator

generate-api:
	oapi-codegen -generate types,chi-server,spec -package generated ./api/oapi.yaml > ./internal/transport/generated/http.gen.go

window.onload = function() {
  //<editor-fold desc="Changeable Configuration Block">

  // get current host and port
  const host = window.location.host;
  let prefix = "https://";
  if (host.indexOf("localhost") > -1) {
    prefix = "http://";
  }

  url = prefix + host + "/swagger.json";



  // the following lines will be replaced by docker/configurator, when it runs in a docker-container
  window.ui = SwaggerUIBundle({
    url: url,
    dom_id: '#swagger-ui',
    deepLinking: true,
    presets: [
      SwaggerUIBundle.presets.apis,
      SwaggerUIStandalonePreset
    ],
    plugins: [
      SwaggerUIBundle.plugins.DownloadUrl
    ],
    layout: "StandaloneLayout"
  });

  //</editor-fold>
};

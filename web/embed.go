package web

import (
	"embed"
)

//go:embed static
var swaggerEmbed embed.FS

func GetSwaggerUI() embed.FS {
	return swaggerEmbed
}

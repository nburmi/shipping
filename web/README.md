[link to docs](https://github.com/swagger-api/swagger-ui/blob/HEAD/docs/usage/installation.md#plain-old-htmlcssjs-standalone)

# Manual installation

TLDR: Basically I just copied the dist folder from the [swagger-ui repo](https://github.com/swagger-api/swagger-ui/tree/master/dist) and edited the swagger-initializer.js file.

## Plain old HTML/CSS/JS (Standalone)

The folder /dist includes all the HTML, CSS and JS files needed to run SwaggerUI on a static website or CMS, without requiring NPM.
    Download the latest release.
    Copy the contents of the /dist folder to your server.
    Open swagger-initializer.js in your text editor and replace "https://petstore.swagger.io/v2/swagger.json" with the URL for your OpenAPI 3.0 spec.

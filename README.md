# Shipping Orders Calculator

## Description

This API calculates the number of packs needed to fulfill an order, ensuring the use of the fewest packs possible while only shipping complete packs. It's written in Golang and is accessible through an HTTP API. The API's interaction is made seamless with the integration of Swagger UI for better visualization and testing.

### Features

- Complete Pack Shipping: Only whole packs are sent. Packs are not broken.
- Efficient Fulfillment: No more items than necessary are sent to fulfill the order.
- Fewest Packs Possible: The API calculates the fewest number of packs needed to fulfill the order.
- Flexible Pack Sizes: The application supports dynamic pack sizes. Pack sizes can be changed, added, or removed without altering the codebase.
- Swagger UI Interaction: A Swagger UI is available for easy interaction, visualization, and testing of the API.


# Swagger

Swagger temporary location https://shipping-orders-nburmi.fly.dev/
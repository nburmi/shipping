package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/nburmi/shipping/internal/calculator"
	"gitlab.com/nburmi/shipping/internal/transport"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	c := calculator.New([]int{250, 500, 1000, 2000, 5000})
	h, err := transport.NewHTTP(c)
	if err != nil {
		log.Fatalf("failed to create http handler: %v", err)
	}

	timeout := time.Second
	srv := http.Server{
		Addr:         ":8080",
		Handler:      h,
		WriteTimeout: timeout,
		ReadTimeout:  timeout,
		IdleTimeout:  timeout,
	}

	shutdownTimeout := time.Second * 5

	go func() {
		<-ctx.Done()

		ctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
		defer cancel()

		err := srv.Shutdown(ctx)
		if err != nil {
			log.Fatalf("failed to shutdown server: %v", err)
		}
	}()

	log.Println("starting server")

	err = srv.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.Fatalf("failed to start server: %v", err)
	}

	log.Println("server stopped")
}
